**Server Version:** X.Y.Z  
**Mod Version:** X.Y.Z  
**Platform:** Windows / Mac / Linux

### Description

Explain the issue you're running into.

### Config

```json
Include your Th3Config.json (optional) might very usefull to reproduce the issue.

MAKE SURE TO NOT INCLUDE ANY SENSITIVE INFORMATION LIKE TOKENS (Discord/Influxdb)

You may also exchange the ip or domain for your influxdb if you don't want to share it (example.com).

```

### How to reproduce

1. Make a '....'
2. Use it on '....'
3. It crashes / doesn't do the thing.

### Logs

```
Paste crash logs from server-main.txt or server-crash.txt here.
```

If the logs get too big you may wanna use [Gitlab Snippets](https://gitlab.com/-/snippets/new) and paste the link in here like so `[Logs](https://gitlab.com/-/snippets/1234....)`
